export default function linmitFunctionCallCount(cd,n){
     // Should return a function that invokes `cb`.
    // The returned function should only allow `cb` to be invoked `n` times.
    // Returning null is acceptable if cb can't be returned
    let limit = n
    return ()=>{
        if (limit >1){
            --limit
            console.log(limit)
             cd()
        }else{
            return ()=>{

            }
        }
    }
}